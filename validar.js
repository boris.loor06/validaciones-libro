let expresionesReg = {
    codigo: /[\w+]{5,}/,
    titulo: /[\w+]{0,100}/,
    autor: /[\w+]{0,60}/,
    editorial: /[\w+]{0,30}/
}



function validarDatos(){
    codigo = document.getElementById('cod').value
    titulo = document.getElementById('title').value
    autor = document.getElementById('autor').value
    editorial = document.getElementById('editorial').value
    mensaje = document.getElementById('msjAlert').value
    fechaPub = document.getElementById('fecha-publicacion').value
    fechaIng = document.getElementById('fecha-ingreso').value

    if(codigo === null || codigo.length === 0 || expresionesReg.codigo.test(codigo)){
        mensaje.innerHTML = `<p>El código debe ser de 5 carácteres</p>`
    }
    if(titulo === null || titulo.length === 0 || expresionesReg.titulo.test(titulo)){
        mensaje.innerHTML = `<p>El titulo debe ser máximo 100 carácteres</p>`
    }
    if(autor === null || autor.length === 0 || expresionesReg.autor.test(autor)){
        mensaje.innerHTML = `<p>El autor tiene un máximo de 60 carácteres</p>`
    }
    if(editorial === null || editorial.length === 0 || expresionesReg.editorial.test(editorial)){
        mensaje.innerHTML = `<p>El editorial tiene un máximo de 30 carácteres</p>`
    }
    if(fechaIng === null || fechaIng.length === 0){
        mensaje.innerHTML = `<p>La fecha de ingreso es obligatoria</p>`
    }
    if(fechaPub === null || fechaPub.length === 0){
        mensaje.innerHTML = `<p>La fecha de publicación es obligatoria</p>`
    }
    if(Date.parse(fechaPub) < Date.parse(fechaIng)){//date parse devuelve los milisegundos de las fechas
        mensaje.innerHTML = `<p>La fecha de ingreso debe ser mayor a la fecha de publicación</p>`
    }
}